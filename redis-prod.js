const express = require('express');
const redis = require('redis');
const morgan = require('morgan');
const { v4: uuidv4 } = require('uuid');

const app = express();
let redisClient;

try {
    redisClient = redis.createClient({
        url: ''
    });
} catch (err) {
    console.log(err);
}

redisClient.connect();

app.use(express.json());
app.use(morgan('combined'));

app.post('/connect', async (req, res, next) => {
    const {
        socketId,
        botId,
    } = req.body;

    const setResult = await redisClient.SADD('my-set', JSON.stringify({ socketId, botId }));

    return res.json({ data: setResult });
});

app.get('/redis-key', async (req, res, next) => {
    const key = req.query.key;

    const data = await redisClient.SET('Hello', 'AWS');


    return res.send('OK');
});

const deleteKeysByPattern = async (pattern) => {
    const script = `
      local keys = redis.call('KEYS', ARGV[1])
      for i = 1, #keys do
        redis.call('DEL', keys[i])
      end
    `;
    return await redisClient.eval(script, {
        arguments: [pattern],
    });
};

app.get('/keys', async (req, res, next) => {
    await deleteKeysByPattern('campaign-statistic:1:customer-calls:*');
    return res.send('OK')
});

app.get('/s', async (req, res, next) => {
    redisClient.SET('A', 1, {});

    return res.send("PK");
})

app.get('/cache', async (req, res, next) => {
    let promises = [];
    let index = 0;
    let campaignId = 1
    let customerId = 1;

    for (let i = 0; i < 10000; ++i) {

        promises.push(
            redisClient.SET(`campaign-statistic:${campaignId}:customer-calls:${customerId++}`, 1),
        );

        if (promises.length == 1000) {
            await Promise.all(promises);
            console.log('DUNK chunk ', index + 1);
            promises = [];
        }
    }

    if (promises.length) {
        await Promise.all(promises);
    }

    return res.json({ message: 'OK' });
});

app.post('/hook', async (req, res, next) => {
    console.log(req.body.data);

    return res.sendStatus(200);
});

app.post('/disconnect', async (req, res, next) => {
    const {
        socketId,
    } = req.body;

    let sockets = await redisClient.SMEMBERS('botSockets');
    let destinationSocket = sockets.find(socket => {
        socket = JSON.parse(socket);

        return socket.socketId == socketId;
    });

    destinationSocket = JSON.parse(destinationSocket);

    console.log(JSON.stringify({ socketId, botId: destinationSocket.botId }));

    const delResult = await redisClient.SREM('my-set', JSON.stringify({ socketId, botId: destinationSocket.botId }));

    return res.json({ data: delResult });
});

app.listen(5001);
