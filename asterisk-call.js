const amqp = require('amqplib/callback_api')
const axios = require('axios');
const getSampleCdr = require('./cdr-helper');

function objectId() {
  const os = require('os');
  const crypto = require('crypto');

  const secondInHex = Math.floor(new Date()/1000).toString(16);
  const machineId = crypto.createHash('md5').update(os.hostname()).digest('hex').slice(0, 6);
  const processId = process.pid.toString(16).slice(0, 4).padStart(4, '0');
  const counter = process.hrtime()[1].toString(16).slice(0, 6).padStart(6, '0');

  return secondInHex + machineId + processId + counter;
}

// Create connection
amqp.connect('amqp://user:password@localhost:5672', (err, conn) => {
  // Create channel
  conn.createChannel((err, ch) => {
    // Name of the queue
    const q = 'handleOutboundCall';
    // Declare the queue
    ch.assertQueue(q, { durable: true }) // { durable: true } ensures that the message will still be redelivered even if RabbitMQ service is turned off/restarted
    // Tell RabbitMQ not to give more than 1 message per worker
    ch.prefetch(30)

    // Wait for Queue Messages
    console.log(` [*] Waiting for messages in ${q}. To exit press CTRL+C`);
    const responses = [500];
    let call = 0;
    ch.consume(q, async msg => {
      // Fake task which simulates execution time
      const date = new Date();
      console.log(`call done! ${call}`);
      call++;
      const url = 'http://localhost:4010/api/v1/outbound-calls/call-end';

      const body = {
        ...JSON.parse(msg.content), cdr: {
          ...JSON.parse(msg.content),
          ...getSampleCdr(date),
          _id: objectId(),
        },
      };

      console.log({ body });
      console.log('==================================================');

      await axios.put(url, body);
      ch.ack(msg);
    }, { noAck: false } // noAck: false means Message acknowledgments is turned on
    )
  })
})

function timeout(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}