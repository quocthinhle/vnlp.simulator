const express = require('express');
const redis = require('redis');
const morgan = require('morgan');
const { v4: uuidv4 } = require('uuid');
const axios = require('axios');

const app = express();
let redisClient;

try {
    redisClient = redis.createClient({});
} catch (err) {
    console.log(err);
}

redisClient.connect();

app.use(express.json());
app.use(morgan('combined'));

app.get('/test', (req, res, next) => {
    console.log((new Date()).toLocaleTimeString());

    return res.sendStatus(200);
});

app.post('/test', async (req, res, next) => {
    await axios({
        method: 'GET',
        url: 'http://localhost:5001/test',
    });

    return res.send('OK');
});

app.post('/add-job', async (req, res, next) => {
    for (let i = 0; i < 9; ++i) {
        await axios({
            method: 'get',
            url: 'http://localhost:5001/test'
        })
    }

    return res.send('OK');
});

app.listen(5001);
