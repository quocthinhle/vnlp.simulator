function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

const sampleCDRs = (date) => [
    {
        "score": 1,
        "tags": [],
        "duration": 21,
        "state": "CONGESTION",
        "caller": "842367109722",
        "createdAt": date,
        "actionCode": "USER_HANGUP"
    },
    {
        "score": 1,
        "tags": [],
        "duration": 23,
        "state": "USER_HANGUP",
        "caller": "842367109722",
        "createdAt": date,
        "actionCode": "USER_HANGUP"
    },
    {
        "score": 1,
        "tags": [],
        "duration": 20,
        "state": "USER_HANGUP",
        "caller": "842367109722",
        "createdAt": date,
        "actionCode": "USER_HANGUP",
    },
];

module.exports = (date) => {
    return sampleCDRs(date)[0];
};